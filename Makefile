#
#
#
IMAGE	:= gitlab-registry.cern.ch/cholm/docker-ubuntu-doxygen


build:	.build

.build:	Dockerfile
	docker build -t $(IMAGE) -f $< .
	touch $@ 

run:	build
	docker run -ti --env=DISPLAY --volume=/tmp/.X11-unix:/tmp/.X11-unix \
		$(IMAGE) bash -l

clean:
	rm *~ .build

.PHONY:	run


#
# EOF
#
