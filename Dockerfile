#
# Build as
#
#   docker build -t gitlab-registry.cern.ch/cholm/cc7-root-doxygen:base .
#
# This sets up a docker image
#
# - Based on CERN CentOS 7
# - With Kerboros client
# - Utility to copy to EOS
# - ROOT (including doxygen tag file)
#
# This accepts a single argument - the ROOT version
#
FROM ubuntu:artful
LABEL maintainer="christian.holm.christensen@cern.ch"

# Install Kerberos, Doxygen (w/LaTeX), Graphviz, Make
COPY xrootd.list storageci.key /etc/apt/sources.list.d/
RUN cat /etc/apt/sources.list.d/storageci.key | apt-key add - \
    && DEBIAN_FRONTEND=noninteractive apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y \
       krb5-user doxygen-latex graphviz make xrootd-client \
  && rm -rf /var/lib/apt/lists/*
  
# Script that will rsync the output generated to an EOS folder (could
# be an EOS Web site)
COPY deploy-eos.sh /usr/local/bin/deploy-eos
RUN chmod 755 /usr/local/bin/deploy-eos

#
# EOF
#

